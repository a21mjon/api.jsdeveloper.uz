-- CreateTable
CREATE TABLE "users" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "fullName" VARCHAR NOT NULL,
    "userName" VARCHAR,
    "otpCode" INTEGER NOT NULL,
    "phone" VARCHAR NOT NULL,
    "updatedAt" TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "chatId" INTEGER NOT NULL,

    CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "users_otpCode_key" ON "users"("otpCode");

-- CreateIndex
CREATE UNIQUE INDEX "users_chatId_key" ON "users"("chatId");

