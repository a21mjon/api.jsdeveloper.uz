export function minutesDiff(dateTimeValue2, dateTimeValue1) {
  let differenceValue = (dateTimeValue2.getTime() - dateTimeValue1.getTime()) / 1000
  differenceValue /= 60

  return Math.abs(Math.round(differenceValue))
}
