import { Action, Ctx, Hears, On, Start, Update } from 'nestjs-telegraf'
import { Context } from 'telegraf'
import { UserService } from '../user/user.service'

@Update()
export class TelegramBotUpdate {
  constructor(private userService: UserService) {}

  @Hears('test')
  test(@Ctx() ctx: Context) {
    console.log(ctx.msg)
  }

  @Start()
  async start(@Ctx() ctx: Context) {
    await ctx.reply('Ro`yxatdan otish uchun pastdagi tugmani bosing.', {
      reply_markup: {
        resize_keyboard: true,
        one_time_keyboard: true,
        keyboard: [[{ text: 'Kontakt yuborish', request_contact: true }]]
      }
    })
  }

  @On('contact')
  async onRequestContact(@Ctx() ctx: Context) {
    const chatId = ctx.chat.id
    const phoneNumber = (ctx.message as any).contact.phone_number
    const user = await this.userService.findByChatId(chatId)

    const otp = this.generateOTP()

    if (user) {
      await this.userService.updateOtpCode(chatId, otp)
    } else {
      await this.userService.create({
        fullName: `${ctx.message.from.first_name} ${ctx.message.from?.last_name || ''}`,
        userName: ctx.message.from.username || '',
        chatId: chatId,
        otpCode: otp,
        phone: phoneNumber
      })
    }

    await this.sendOtpCode(ctx, otp)
  }

  @Action('refresh_otp')
  async refreshOtp(ctx: Context) {
    const chatId = ctx.chat.id

    const otp = this.generateOTP()

    await this.userService.updateOtpCode(chatId, otp)

    await this.sendOtpCode(ctx, otp)
  }

  private async sendOtpCode(ctx, otp) {
    await ctx.reply('🔒Sizning 2 daqiqalik kodingiz: \n\n' + `\`\`\`${otp}\`\`\``, {
      parse_mode: 'MarkdownV2',
      reply_markup: {
        inline_keyboard: [[{ text: 'Kodnig yangilash 🔄', callback_data: 'refresh_otp' }]]
      }
    })
  }

  private generateOTP(): number {
    const optLength: number = 4
    const digits: string = '0123456789'

    let OTP = ''

    for (let i = 0; i < optLength; i++) {
      OTP += digits[Math.floor(Math.random() * 10)]
    }

    return Number(OTP)
  }
}
