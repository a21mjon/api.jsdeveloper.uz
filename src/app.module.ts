import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { UserModule } from './user/user.module'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { AuthModule } from './auth/auth.module'
import { TelegrafModule } from 'nestjs-telegraf'
import { TelegramBotUpdate } from './telegram-bot/telegram-bot.update'
import { DatabaseModule } from './database/database.module'

@Module({
  imports: [
    ConfigModule.forRoot(),
    DatabaseModule,
    TelegrafModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          token: configService.get('BOT_TOKEN')
        }
      }
    }),
    UserModule,
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService, TelegramBotUpdate]
})
export class AppModule {}
