import { Body, Controller, HttpStatus, Post, Res, UnauthorizedException } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { AuthService } from './auth.service'
import { CheckOtpDto } from './dto/check-otp.dto'

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/check-credentials')
  async checkCredentials(@Body() body: CheckOtpDto, @Res() res: Response) {
    const { access_token, otpTimeOut } = await this.authService.checkUserOtp(body.otp)

    if (access_token === '') {
      throw new UnauthorizedException()
    }

    if (otpTimeOut > 2) {
      return res.status(HttpStatus.NOT_FOUND).json({
        message: 'Kod eskirgan. Kodni yangilang.'
      })
    }

    return res.status(HttpStatus.OK).json({
      access_token
    })
  }

  @Post('/sign-out')
  signOut(@Res() res: Response) {
    return res.status(HttpStatus.OK).json({
      message: 'Successfully signed out!'
    })
  }
}
