import { ApiProperty } from '@nestjs/swagger'

export class CheckOtpDto {
  @ApiProperty()
  otp: number
}
