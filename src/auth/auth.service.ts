import { Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { UserService } from '../user/user.service'
import { minutesDiff } from '../shared/functions'

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService
  ) {}

  async checkUserOtp(otp: number) {
    const { hasUser, user } = await this.userService.checkOtp(otp)

    return hasUser
      ? {
          access_token: await this.jwtService.signAsync({
            id: user.id,
            chatId: user.chatId
          }),
          otpTimeOut: this.checkOtpCodeTime(user.updatedAt)
        }
      : {
          access_token: ''
        }
  }

  private checkOtpCodeTime(userUpdatedDate: Date): number {
    const now = new Date()
    const updatedAt = new Date(userUpdatedDate)

    const minusedHours = now.getHours() - updatedAt.getHours()

    updatedAt.setHours(updatedAt.getHours() - minusedHours)

    return minutesDiff(now, updatedAt)
  }
}
