import { User as UserEntity } from '@prisma/client'
import { Exclude } from 'class-transformer'

export class UserResponse implements UserEntity {
  constructor(partial: Partial<UserEntity>) {
    Object.assign(this, partial)
  }

  id: number
  fullName: string
  userName: string
  phone: string
  createdAt: Date

  @Exclude()
  chatId: number

  @Exclude()
  otpCode: number

  @Exclude()
  updatedAt: Date
}
