import { Injectable } from '@nestjs/common'
import { CreateUserDto } from './dto/create-user.dto'
import { PrismaService } from '../database/prisma.service'

@Injectable()
export class UserService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createUserDto: CreateUserDto) {
    return this.prisma.user.create({
      data: createUserDto
    })
  }

  async findByChatId(chatId: number) {
    return this.prisma.user.findUnique({
      where: {
        chatId: chatId
      }
    })
  }

  async checkOtp(otpCode: number) {
    const user = await this.prisma.user.findUnique({
      where: {
        otpCode
      }
    })

    return {
      hasUser: !!user,
      user
    }
  }

  async updateOtpCode(chatId: number, otpCode: number) {
    return this.prisma.user.update({
      where: {
        chatId: chatId
      },
      data: {
        otpCode,
        updatedAt: new Date()
      }
    })
  }
}
