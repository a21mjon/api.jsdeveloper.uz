import { ApiProperty } from '@nestjs/swagger'

export class CreateUserDto {
  @ApiProperty()
  fullName: string

  @ApiProperty()
  userName: string

  @ApiProperty()
  chatId: number

  @ApiProperty()
  otpCode: number

  @ApiProperty()
  phone: string
}
